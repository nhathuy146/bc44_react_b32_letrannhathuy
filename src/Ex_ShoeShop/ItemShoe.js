import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { data, handleWatchDetail, handleBuy } = this.props;
    let { image, name, shortDescription } = data;
    return (
      <div className="col-6 my-4">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{shortDescription}</p>
          </div>
          <button
            onClick={() => {
              handleWatchDetail(data);
            }}
            className="btn btn-success"
          >
            Xem chi tiết
          </button>
          <button
            onClick={() => {
              handleBuy(data);
            }}
            className="btn btn-primary"
          >
            Mua
          </button>
        </div>
      </div>
    );
  }
}
