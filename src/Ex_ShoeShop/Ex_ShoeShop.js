import React, { Component } from "react";
import { shoeArr } from "./data";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import CartShoe from "./CartShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleViewDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id == shoe.id);
    if (index == -1) {
      // Chưa có trong giỏ hàng
      // Copy shoe vào newShoe và thêm key soLUong
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      // Có trong giỏ hàng
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };

  handleChangeQuantity = (idShoe, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id == idShoe);
    cloneCart[index].soLuong += option;
    // Nếu số lượng =0 thì xóa
    if (cloneCart[index].soLuong == 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };

  handleDelete = (idShoe) => {
    let cloneCart = this.state.cart.filter((item) => item.id !== idShoe);
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="row">
        <CartShoe
          handleRemove={this.handleDelete}
          cart={this.state.cart}
          handleChangeQuantity={this.handleChangeQuantity}
        />
        <ListShoe
          handleBuy={this.handleAddToCart}
          handleViewDetail={this.handleViewDetail}
          list={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detailShoe} />
      </div>
    );
  }
}
